package ru.rastorguev.tm.error;

public class EntityDuplicateException extends RuntimeException{

    public EntityDuplicateException(String message) {
        super(message);
    }
}