package ru.rastorguev.tm.command;

import ru.rastorguev.tm.context.Bootstrap;
import ru.rastorguev.tm.entity.Project;

import java.io.IOException;

import static ru.rastorguev.tm.view.View.*;

public class ProjectSelectCommand extends AbstractCommand{

    public ProjectSelectCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project_select";
    }

    @Override
    public String getDescription() {
        return "Select exact project.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Project select");
        System.out.println("Enter project ID");
        printAllProjects(bootstrap.getProjectService().findAll());
        Project project = bootstrap.getProjectService().findOne(bootstrap.getProjectService().getProjectIdByNumber(Integer.parseInt(reader.readLine())));
        printProject(project);
        System.out.println("OK");
    }
}