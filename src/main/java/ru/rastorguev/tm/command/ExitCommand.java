package ru.rastorguev.tm.command;

import ru.rastorguev.tm.context.Bootstrap;

import java.io.IOException;

public class ExitCommand extends AbstractCommand{

    public ExitCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Exit app.";
    }

    @Override
    public void execute() throws IOException {
        System.exit(0);
    }
}