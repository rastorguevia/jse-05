package ru.rastorguev.tm.command;

import ru.rastorguev.tm.context.Bootstrap;
import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.enumerated.Confirmation;

import java.io.IOException;
import java.util.List;

import static ru.rastorguev.tm.util.DateUtil.*;
import static ru.rastorguev.tm.view.View.*;

public class TaskEditCommand extends AbstractCommand{

    public TaskEditCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task_edit";
    }

    @Override
    public String getDescription() {
        return "Edit selected task.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Task edit");
        System.out.println("Enter Project ID");
        printAllProjects(bootstrap.getProjectService().findAll());
        String projectId = bootstrap.getProjectService().getProjectIdByNumber(Integer.parseInt(reader.readLine()));
        printTaskListByProjectId(projectId, bootstrap.getTaskService().findAll());
        List<Task> filteredTaskList = bootstrap.getTaskService().filterTaskListByProjectId(projectId, bootstrap.getTaskService().findAll());
        System.out.println("Enter Task ID");
        String taskId = bootstrap.getTaskService().getTaskIdByNumber(Integer.parseInt(reader.readLine()),filteredTaskList);
        Task task = bootstrap.getTaskService().findOne(taskId);
        Task editedTask = new Task(task.getProjectId());
        editedTask.setId(task.getId());
        System.out.println("Edit name? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter task name");
            editedTask.setName(reader.readLine());
        } else editedTask.setName(task.getName());
        System.out.println("Edit description? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter new description");
            editedTask.setDescription(reader.readLine());
        } else editedTask.setDescription(task.getDescription());
        System.out.println("Edit start date? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter start date");
            editedTask.setStartDate(stringToDate(reader.readLine()));
        } else editedTask.setStartDate(task.getStartDate());
        System.out.println("Edit end date? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter end date");
            editedTask.setEndDate(stringToDate(reader.readLine()));
        } else editedTask.setEndDate(task.getEndDate());
        bootstrap.getTaskService().merge(editedTask);
        System.out.println("OK");
    }
}