package ru.rastorguev.tm.command;

import ru.rastorguev.tm.context.Bootstrap;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.enumerated.Confirmation;

import java.io.IOException;

import static ru.rastorguev.tm.util.DateUtil.*;
import static ru.rastorguev.tm.view.View.*;

public class ProjectEditCommand extends AbstractCommand{

    public ProjectEditCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project_edit";
    }

    @Override
    public String getDescription() {
        return "Edit selected project.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Project edit");
        System.out.println("Enter project ID");
        printAllProjects(bootstrap.getProjectService().findAll());
        Project project = bootstrap.getProjectService().findOne(bootstrap.getProjectService().getProjectIdByNumber(Integer.parseInt(reader.readLine())));
        Project editedProject = new Project();
        editedProject.setId(project.getId());
        System.out.println("Edit name? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter project name");
            editedProject.setName(reader.readLine());
        } else editedProject.setName(project.getName());
        System.out.println("Edit description? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter new description");
            editedProject.setDescription(reader.readLine());
        } else editedProject.setDescription(project.getDescription());
        System.out.println("Edit start date? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter start date");
            editedProject.setStartDate(stringToDate(reader.readLine()));
        } else editedProject.setStartDate(project.getStartDate());
        System.out.println("Edit end date? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Enter end date");
            editedProject.setEndDate(stringToDate(reader.readLine()));
        } else editedProject.setEndDate(project.getEndDate());
        bootstrap.getProjectService().merge(editedProject);
        System.out.println("OK");
    }
}