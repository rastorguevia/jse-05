package ru.rastorguev.tm.command;

import ru.rastorguev.tm.context.Bootstrap;

import java.io.IOException;

import static ru.rastorguev.tm.view.View.*;

public class TaskClearCommand extends AbstractCommand{

    public TaskClearCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task_clear";
    }

    @Override
    public String getDescription() {
        return "Remove project tasks.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Enter Project ID");
        printAllProjects(bootstrap.getProjectService().findAll());
        String projectId = bootstrap.getProjectService().getProjectIdByNumber(Integer.parseInt(reader.readLine()));
        bootstrap.getTaskService().removeTaskListByProjectId(projectId);
        System.out.println("OK");
    }
}