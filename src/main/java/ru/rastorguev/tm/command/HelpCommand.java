package ru.rastorguev.tm.command;

import ru.rastorguev.tm.context.Bootstrap;

import java.util.LinkedList;
import java.util.List;

public class HelpCommand extends AbstractCommand{

    public HelpCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() { return "Show all commands."; }

    @Override
    public void execute() {
        List<AbstractCommand> AbstractCommandList = new LinkedList<>(bootstrap.getCommands().values());
        for (final AbstractCommand command : AbstractCommandList) {
            System.out.println(command.getName() + ": " + command.getDescription());
        }
    }
}