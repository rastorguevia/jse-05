package ru.rastorguev.tm.command;

import ru.rastorguev.tm.context.Bootstrap;

import java.io.IOException;

import static ru.rastorguev.tm.view.View.*;

public class ProjectRemoveCommand extends AbstractCommand{

    public ProjectRemoveCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project_remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Project remove");
        System.out.println("Enter ID");
        printAllProjects(bootstrap.getProjectService().findAll());
        String projectId = bootstrap.getProjectService().getProjectIdByNumber(Integer.parseInt(reader.readLine()));
        bootstrap.getProjectService().remove(projectId);
        System.out.println("OK");
    }
}