package ru.rastorguev.tm.command;

import ru.rastorguev.tm.context.Bootstrap;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.enumerated.Confirmation;

import java.io.IOException;

import static ru.rastorguev.tm.util.DateUtil.*;

public class ProjectCreateCommand extends AbstractCommand{

    public ProjectCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project_create";
    }

    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Project create");
        System.out.println("Enter name");
        Project project = new Project();
        project.setName(reader.readLine());
        System.out.println("Enter description");
        project.setDescription(reader.readLine());
        System.out.println("Add date? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Add start date DD.MM.YYYY? Y/N");
            if (Confirmation.Y.equals(Confirmation.valueOf(reader.readLine().toUpperCase()))) {
                System.out.println("Enter start date");
                project.setStartDate(stringToDate(reader.readLine()));
            }
            System.out.println("Add end date DD.MM.YYYY? Y/N");
            if (Confirmation.Y.equals(Confirmation.valueOf(reader.readLine().toUpperCase()))) {
                System.out.println("Enter end date");
                project.setEndDate(stringToDate(reader.readLine()));
            }
        }
        bootstrap.getProjectService().persist(project);
        System.out.println("OK");
    }
}