package ru.rastorguev.tm.command;

import ru.rastorguev.tm.context.Bootstrap;

import java.io.IOException;

import static ru.rastorguev.tm.view.View.*;

public class TaskListCommand extends AbstractCommand{

    public TaskListCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task_list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Task list");
        System.out.println("Enter Project ID");
        printAllProjects(bootstrap.getProjectService().findAll());
        String projectId = bootstrap.getProjectService().getProjectIdByNumber(Integer.parseInt(reader.readLine()));
        printTaskListByProjectId(projectId, bootstrap.getTaskService().findAll());
        System.out.println("OK");
    }
}