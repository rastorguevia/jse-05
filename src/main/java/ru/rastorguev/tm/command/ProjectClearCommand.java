package ru.rastorguev.tm.command;

import ru.rastorguev.tm.context.Bootstrap;

public class ProjectClearCommand extends AbstractCommand{

    public ProjectClearCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project_clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        bootstrap.getProjectService().removeAll();
        System.out.println("OK");
    }
}