package ru.rastorguev.tm.command;

import ru.rastorguev.tm.context.Bootstrap;

import static ru.rastorguev.tm.view.View.*;

public class ProjectListCommand extends AbstractCommand{

    public ProjectListCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project_list";
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        System.out.println("Project list");
        printAllProjects(bootstrap.getProjectService().findAll());
        System.out.println("OK");
    }
}