package ru.rastorguev.tm.command;

import ru.rastorguev.tm.context.Bootstrap;
import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.enumerated.Confirmation;

import java.io.IOException;

import static ru.rastorguev.tm.util.DateUtil.*;
import static ru.rastorguev.tm.view.View.*;

public class TaskCreateCommand extends AbstractCommand{

    public TaskCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task_create";
    }

    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("Task create");
        System.out.println("Enter Project ID");
        printAllProjects(bootstrap.getProjectService().findAll());
        Task task = new Task(bootstrap.getProjectService().getProjectIdByNumber(Integer.parseInt(reader.readLine())));
        System.out.println("Enter task name");
        task.setName(reader.readLine());
        System.out.println("Enter task description");
        task.setDescription(reader.readLine());
        System.out.println("Add date? Y/N");
        if (Confirmation.Y.equals(Confirmation.valueOf(reader.readLine().toUpperCase()))) {
            System.out.println("Add start date DD.MM.YYYY? Y/N");
            if (Confirmation.Y.equals(Confirmation.valueOf(reader.readLine().toUpperCase()))) {
                System.out.println("Enter start date");
                task.setStartDate(stringToDate(reader.readLine()));
            }
            System.out.println("Add end date DD.MM.YYYY? Y/N");
            if (Confirmation.Y.equals(Confirmation.valueOf(reader.readLine().toUpperCase()))) {
                System.out.println("Enter end date");
                task.setEndDate(stringToDate(reader.readLine()));
            }
        }
        bootstrap.getTaskService().persist(task);
        System.out.println("OK");
    }
}