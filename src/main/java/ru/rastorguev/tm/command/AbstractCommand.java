package ru.rastorguev.tm.command;

import ru.rastorguev.tm.context.Bootstrap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public abstract class AbstractCommand {
    protected Bootstrap bootstrap;
    protected final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public AbstractCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String getName();
    public abstract String getDescription();
    public abstract void execute() throws IOException;
}