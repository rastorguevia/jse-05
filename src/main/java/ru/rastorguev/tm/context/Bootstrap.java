package ru.rastorguev.tm.context;

import static ru.rastorguev.tm.view.View.*;

import ru.rastorguev.tm.command.*;
import ru.rastorguev.tm.repository.ProjectRepository;
import ru.rastorguev.tm.repository.TaskRepository;
import ru.rastorguev.tm.service.ProjectService;
import ru.rastorguev.tm.service.TaskService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class Bootstrap {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final TaskRepository taskRepository = new TaskRepository();
    private final TaskService taskService = new TaskService(taskRepository);
    private final ProjectRepository projectRepository = new ProjectRepository(taskRepository);
    private final ProjectService projectService = new ProjectService(projectRepository);
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public void init() throws IOException {
        commandRegistry(new HelpCommand(this));
        commandRegistry(new ProjectClearCommand(this));
        commandRegistry(new ProjectCreateCommand(this));
        commandRegistry(new ProjectEditCommand(this));
        commandRegistry(new ProjectListCommand(this));
        commandRegistry(new ProjectRemoveCommand(this));
        commandRegistry(new ProjectSelectCommand(this));
        commandRegistry(new TaskClearCommand(this));
        commandRegistry(new TaskCreateCommand(this));
        commandRegistry(new TaskEditCommand(this));
        commandRegistry(new TaskListCommand(this));
        commandRegistry(new TaskRemoveCommand(this));
        commandRegistry(new TaskSelectCommand(this));
        commandRegistry(new ExitCommand(this));

        launchConsole();
    }

    private void commandRegistry(AbstractCommand command) {
        commands.put(command.getName(), command);
    }

    public void launchConsole() throws IOException {
        showWelcomeMsg();

        while (true) {
            String input = reader.readLine();
            final AbstractCommand command = commands.get(input.toLowerCase());
            if (command != null) command.execute();
            if (command == null) showUnknownCommandMsg();
        }
    }
}