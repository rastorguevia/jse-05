package ru.rastorguev.tm.service;

import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.repository.TaskRepository;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class TaskService {
    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Collection<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task findOne(String taskId) {
        if (taskId == null || taskId.isEmpty()) return null;
        return taskRepository.findOne(taskId);
    }

    public Task persist(Task task) {
        if (task == null) return null;
        return taskRepository.persist(task);
    }

    public Task merge(Task task) {
        if (task == null) return null;
        return taskRepository.merge(task);
    }

    public Task remove(String taskId) {
        if (taskId == null || taskId.isEmpty()) return null;
        return taskRepository.remove(taskId);
    }

    public void removeAll() {
        taskRepository.removeAll();
    }

    public List<Task> filterTaskListByProjectId(String projectId, Collection<Task> taskCollection) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskCollection == null) return null;
        List<Task> listOfTasks = new LinkedList<>();
        listOfTasks.addAll(taskCollection);
        for (int i = 0; i < listOfTasks.size(); i++) {
            if (!listOfTasks.get(i).getProjectId().equals(projectId)) {
                listOfTasks.remove(i);
            }
        }
        return listOfTasks;
    }

    public String getTaskIdByNumber(int number, List<Task> filteredListOfTasks) {
        if (filteredListOfTasks == null) return null;
        for (Task task: filteredListOfTasks) {
            if (task.getId().equals(filteredListOfTasks.get(number - 1).getId())) {
                return task.getId();
            }
        }
        return null;
    }

    public Task removeTaskListByProjectId(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        List<Task> listOfTasks = new LinkedList<>();
        listOfTasks.addAll(taskRepository.findAll());
        for (Task task: listOfTasks) {
            if (task.getProjectId().equals(projectId)) {
                taskRepository.remove(task.getId());
            }
        }
        return null;
    }
}