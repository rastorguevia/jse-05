package ru.rastorguev.tm.service;

import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.repository.ProjectRepository;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class ProjectService {
    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Collection<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project findOne(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return projectRepository.findOne(projectId);
    }

    public Project persist(Project project) {
        if (project == null) return null;
        return projectRepository.persist(project);
    }

    public Project merge(Project project) {
        if (project == null) return null;
        return projectRepository.merge(project);
    }

    public Project remove(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return projectRepository.remove(projectId);
    }

    public void removeAll() {
        projectRepository.removeAll();
    }

    public String getProjectIdByNumber (int number) {
        List<Project> listOfProjects = new LinkedList<>();
        listOfProjects.addAll(projectRepository.findAll());
        for (Project project : listOfProjects) {
            if (project.getId().equals(listOfProjects.get(number - 1).getId())){
                return project.getId();
            }
        }
        return null;
    }
}