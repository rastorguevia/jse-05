package ru.rastorguev.tm.repository;

import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.error.EntityDuplicateException;

import java.util.*;

public class TaskRepository {
    private final Map<String, Task> taskMap = new LinkedHashMap<>();

    public Collection<Task> findAll() {
        return taskMap.values();
    }

    public Task findOne(String taskId) {
        return taskMap.get(taskId);
    }

    public Task persist(Task task) {
        if (taskMap.containsKey(task.getId())) throw new EntityDuplicateException("Task already exist");
        return taskMap.put(task.getId(), task);
    }

    public Task merge(Task task) {
        taskMap.put(task.getId(), task);
        return task;

    }

    public Task remove(String taskId) {
        return taskMap.remove(taskId);
    }

    public void removeAll() {
        taskMap.clear();
    }
}