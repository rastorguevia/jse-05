package ru.rastorguev.tm.entity;

import java.util.Date;

import static ru.rastorguev.tm.util.DateUtil.*;

public class Task extends AbstractEntity {
    private final String projectId;
    private String name = "";
    private String description = "";
    private Date startDate = stringToDate(dateFormatter.format(new Date()));
    private Date endDate = stringToDate(dateFormatter.format(new Date()));

    public Task(String projectId) {
        this.projectId = projectId;
    }

    public String getProjectId() {
        return projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}